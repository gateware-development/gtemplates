#!/bin/sh
cd ../../

for dir in */; do
	if test -f "$dir/CMakeLists.txt"; then
    cd $dir
	mkdir -p build
	cd build

	mkdir -p debug
	cd debug
	cmake  ./../../ -G "CodeLite - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug
	cd ../

	mkdir -p release
	cd release
	cmake ./../../ -G "CodeLite - Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
    cd ../

    cd ../../
	fi
done