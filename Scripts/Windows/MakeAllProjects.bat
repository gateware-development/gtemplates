@echo off
cd ../../

FOR /R ./ %%G in (.) do (
@echo off 
if exist %%G/CMakeLists.txt ( 
@echo on 
cmake -S %%G -B %%G/build 
))