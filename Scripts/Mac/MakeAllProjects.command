#!/bin/sh
cd ../../

for dir in */; do
	if test -f "$dir/CMakeLists.txt"; then
    cd $dir
	mkdir -p build
	cd build

	cmake ./../ -DCMAKE_OSX_ARCHITECTURES="x86_64" -G "Xcode"
	
    cd ../../
	fi
done