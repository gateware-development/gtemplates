// Simple basecode showing how to create a window and attach a GRasterSurface
#define GATEWARE_ENABLE_CORE // All libraries need this
#define GATEWARE_ENABLE_SYSTEM // Graphics libs require system level libraries
#define GATEWARE_ENABLE_GRAPHICS // Enables all Graphics Libraries
// Ignore some GRAPHICS libraries we aren't going to use
#define GATEWARE_DISABLE_GDIRECTX11SURFACE // we have another template for this
#define GATEWARE_DISABLE_GVULKANSURFACE // we have another template for this
#define GATEWARE_DISABLE_GDIRECTX12SURFACE // we have another template for this
#define GATEWARE_DISABLE_GOPENGLSURFACE // we have another template for this
// With what we want & what we don't defined we can include the API
#include "../Gateware/Gateware.h"
#include "renderer.h"
// open some namespaces to compact the code a bit
using namespace GW;
using namespace CORE;
using namespace SYSTEM;
using namespace GRAPHICS;
// allocate an 800x600 pixel buffer for display (keep centered on Resize)
unsigned int XRGB[800*600] = {0,};
// lets pop a window and use GRasterSurface to upload an image with a triangle
int main()
{
	GWindow win;
	GEventResponder msgs;
	GRasterSurface raster;
	if (+win.Create(0, 0, 820, 620, GWindowStyle::WINDOWEDBORDERED))
	{
        unsigned int color = 0xFFFFFF00;
		msgs.Create([&](const GW::GEvent& e) {
			GW::SYSTEM::GWindow::Events q;
			if (+e.Read(q) && q == GWindow::Events::RESIZE)
            {// Clear the screen broder to a swapping color
				color = ~color;
            }
		});
		win.Register(msgs);
		if (+raster.Create(win))
		{
			Renderer renderer(win, raster);
			// if we had dynamic resolution or animations we would place this inside the loop
			for (int i = 0; i < 800 * 600; i++)
				XRGB[i] = 0xFFFA88FF; // pink
			renderer.Render(XRGB, 800, 600); 
            while (+win.ProcessWindowEvents())
			{
				raster.Clear(color);
                raster.SmartUpdateSurface(XRGB, 800*600, 800, ALIGN_X_CENTER | ALIGN_Y_CENTER);
				raster.Present();
			}
		}
	}
	return 0; // that's all folks
}
