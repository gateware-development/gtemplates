// minimalistic code to draw a single triangle, this is not part of the API.

// Creation, Rendering & Cleanup
class Renderer
{
	// proxy handles
	GW::SYSTEM::GWindow win;
	GW::GRAPHICS::GRasterSurface ras;
	// what we need at a minimum to draw a triangle
	float verts[6] = 
	{
		   0,   0.5f,
		 0.5f, -0.5f,
		-0.5f, -0.5f
	};
public:
	Renderer(GW::SYSTEM::GWindow _win, GW::GRAPHICS::GRasterSurface _ras)
	{
		win = _win;
		ras = _ras;
	}
	void Render(unsigned int* _xrgb, unsigned int _w, unsigned int _h)
	{
		// setup the screen coordinates (NDC -> SCREEN)
        int A[2] = { static_cast<int>((verts[0] + 1.0f) * (_w >> 1)), static_cast<int>((1.0f - verts[1]) * (_h >> 1)) };
        int B[2] = { static_cast<int>((verts[2] + 1.0f) * (_w >> 1)), static_cast<int>((1.0f - verts[3]) * (_h >> 1)) };
        int C[2] = { static_cast<int>((verts[4] + 1.0f) * (_w >> 1)), static_cast<int>((1.0f - verts[5]) * (_h >> 1)) };
		// now we can draw, simple flat bottom triangle logic
		float r = 0;
		float ri = 1.0f / static_cast<float>(B[1] - A[1]);
		for (int y = A[1]; y < B[1]; ++y, r += ri)
		{
			int xS = (C[0] - A[0]) * r + A[0] + 0.5f;
			int xE = (B[0] - A[0]) * r + A[0] + 0.5f;
			for (; xS < xE; ++xS)
				_xrgb[y * _w + xS] = ~0xFF800080;
		}
	}
	~Renderer()
	{
		// nothing to delete
	}
};
