
cmake_minimum_required(VERSION 3.10)

project(PinkScreen)

# currently using unicode in some libraries on win32 but will change soon
ADD_DEFINITIONS(-DUNICODE)
ADD_DEFINITIONS(-D_UNICODE)

if (WIN32)
    add_executable (PinkScreen main.cpp renderer.h)
endif(WIN32)

if (UNIX AND NOT APPLE)
     set(CMAKE_CXX_STANDARD 17)
     set(CMAKE_CXX_STANDARD_REQUIRED ON)

     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -lX11")
     find_package(X11)
     include_directories(${X11_INCLUDE_DIR})
     link_libraries(${X11_LIBRARIES})
     add_executable (PinkScreen main.cpp renderer.h)
endif()

if(APPLE)
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -fmodules -fcxx-modules")
   set(Architecture ${CMAKE_OSX_ARCHITECTURES})
   add_executable (PinkScreen main.mm)
endif(APPLE)
