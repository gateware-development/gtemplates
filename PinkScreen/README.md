# PinkScreen

A raw pixel to screen Template for Windows/Linux. (Mac currently doesn't render. This is a known issue and will be fixed in the future.)

Linux requires X11 and Mac needs cocos. See "Gateware/README.md" for more details.

*if you want to start adding additional libraries be sure to check 
"Gateware/README.md" for what additional dependencies you may need*

